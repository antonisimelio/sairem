# SAIREM4S

#######################################
### Write functions

## Address 0
record(longout, "$(P)$(R)PosX-S") {
    field(DESC, "Set X Position for Manual only")
    field(LOPR, "0")
    field(HOPR, "10000")
    field(DRVL, "0")
    field(DRVH, "10000")
    field(LOW, "2000")
    field(HIGH, "8000")
    field(LSV, "MINOR")
    field(HSV, "MINOR")
    field(DTYP, "asynUInt32Digital")
    field(OUT,  "@asynMask($(PORT_NAME)-WRITE, 0, 0xFFFF, 1000)MODBUS_DATA")
    info(autosaveFields, "VAL")
}

## Address 1
record(longout, "$(P)$(R)PosY-S") {
    field(DESC, "Set Y Position for Manual only")
    field(LOPR, "0")
    field(HOPR, "10000")
    field(DRVL, "0")
    field(DRVH, "10000")
    field(LOW, "2000")
    field(HIGH, "8000")
    field(LSV, "MINOR")
    field(HSV, "MINOR")
    field(DTYP, "asynUInt32Digital")
    field(OUT,  "@asynMask($(PORT_NAME)-WRITE, 1, 0xFFFF, 1000)MODBUS_DATA")
    info(autosaveFields, "VAL")
}


## Address 2
record(mbboDirect, "$(P)$(R)SetupByte") {
    field(DESC, "Configuration and control")
    field(DTYP, "asynUInt32Digital")
    field(OUT,  "@asynMask($(PORT_NAME)-WRITE, 2, 0xFFFF, 1000)MODBUS_DATA")
    info(autosaveFields, "VAL")
}

record(bo, "$(P)$(R)ModeX") {
    field(DESC, "Channel X Mode Control")
    field(DTYP, "asynUInt32Digital")
    field(OUT,  "@asynMask($(PORT_NAME)-WRITE, 2, 0x0001, 1000)MODBUS_DATA")
    field(ZNAM, "Automatic")
    field(ONAM, "Manual")
}

record(bo, "$(P)$(R)ModeY") {
    field(DESC, "Channel Y Mode Control")
    field(DTYP, "asynUInt32Digital")
    field(OUT,  "@asynMask($(PORT_NAME)-WRITE, 2, 0x0002, 1000)MODBUS_DATA")
    field(ZNAM, "Automatic")
    field(ONAM, "Manual")
}

record(bo, "$(P)$(R)StopX") {
    field(DESC, "Channel X Stop request")
    field(DTYP, "asynUInt32Digital")
    field(OUT,  "@asynMask($(PORT_NAME)-WRITE, 2, 0x0004, 1000)MODBUS_DATA")
    field(ZNAM, "Run")
    field(ONAM, "Stop")
}

record(bo, "$(P)$(R)StopY") {
    field(DESC, "Channel Y Stop request")
    field(DTYP, "asynUInt32Digital")
    field(OUT,  "@asynMask($(PORT_NAME)-WRITE, 2, 0x0008, 1000)MODBUS_DATA")
    field(ZNAM, "Run")
    field(ONAM, "Stop")
}

record(bo, "$(P)$(R)Rst") {
    field(DESC, "Reset Faults")
    field(DTYP, "asynUInt32Digital")
    field(OUT,  "@asynMask($(PORT_NAME)-WRITE, 2, 0x0010, 1000)MODBUS_DATA")
    field(ZNAM, "PulseOFF")
    field(ONAM, "PulseON")
    field(HIGH, "1")
}

record(bo, "$(P)$(R)GainXmodif") {
    field(DESC, "Channel X Gain Modif. Authorization")
    field(DTYP, "asynUInt32Digital")
    field(OUT,  "@asynMask($(PORT_NAME)-WRITE, 2, 0x0020, 1000)MODBUS_DATA")
}

record(bo, "$(P)$(R)GainYmodif") {
    field(DESC, "Channel Y Gain Modif. Authorization")
    field(DTYP, "asynUInt32Digital")
    field(OUT,  "@asynMask($(PORT_NAME)-WRITE, 2, 0x0040, 1000)MODBUS_DATA")
}

# Address 9
record(longout, "$(P)$(R)Gain-S") {
    field(DESC, "Set gain value")
    field(LOPR, "1000")
    field(HOPR, "2000")
    field(DRVL, "1000")
    field(DRVH, "2000")
    field(DTYP, "asynUInt32Digital")
    field(OUT,  "@asynMask($(PORT_NAME)-WRITE, 9, 0xFFFF, 1000)MODBUS_DATA")
    info(autosaveFields, "VAL")
}

#######################################
### Read functions

## Address 100
record(longin, "$(P)$(R)PosX-R") {
    field(DESC, "Get X Position")
    field(SCAN, "I/O Intr")
    field(LOPR, "0")
    field(HOPR, "10000")
    field(LOW, "2000")
    field(HIGH, "8000")
    field(LSV, "MINOR")
    field(HSV, "MINOR")
    field(DTYP, "asynUInt32Digital")
    field(INP,  "@asynMask($(PORT_NAME)-READ, 0, 0xFFFF, 1000)MODBUS_DATA")
}

## Address 101
record(longin, "$(P)$(R)PosY-R") {
    field(DESC, "Get Y Position")
    field(SCAN, "I/O Intr")
    field(LOPR, "0")
    field(HOPR, "10000")
    field(LOW, "2000")
    field(HIGH, "8000")
    field(LSV, "MINOR")
    field(HSV, "MINOR")
    field(DTYP, "asynUInt32Digital")
    field(INP,  "@asynMask($(PORT_NAME)-READ, 1, 0xFFFF, 1000)MODBUS_DATA")
}

## Address 102
record(longin, "$(P)$(R)PosXDiscr-R") {
    field(DESC, "X discriminator readout")
    field(SCAN, "I/O Intr")
    field(LOPR, "0")
    field(HOPR, "10000")
    field(DTYP, "asynUInt32Digital")
    field(INP,  "@asynMask($(PORT_NAME)-READ, 2, 0xFFFF, 1000)MODBUS_DATA")
}

## Address 103
record(longin, "$(P)$(R)PosYDiscr-R") {
    field(DESC, "Y discriminator readout")
    field(SCAN, "I/O Intr")
    field(LOPR, "0")
    field(HOPR, "10000")
    field(DTYP, "asynUInt32Digital")
    field(INP,  "@asynMask($(PORT_NAME)-READ, 3, 0xFFFF, 1000)MODBUS_DATA")
}

## Address 104
record(mbbiDirect, "$(P)$(R)FaultByte") {
    field(DESC, "Default status")
    field(SCAN, "I/O Intr")
    field(DTYP, "asynUInt32Digital")
    field(INP,  "@asynMask($(PORT_NAME)-READ, 4, 0xFFFF, 1000)MODBUS_DATA")
}

record(bi, "$(P)$(R)YOverCurr") {
    field(DESC, "Y Channel Over Current")
    field(ONAM, "OK")
    field(ZNAM, "FAULT!")
    field(DTYP, "asynUInt32Digital")
    field(INP,  "@asynMask($(PORT_NAME)-READ, 4, 0x0001, 1000)MODBUS_DATA")
    field(ZSV, "MAJOR")
}

record(bi, "$(P)$(R)XOverCurr") {
    field(DESC, "X Channel Over Current")
    field(ONAM, "OK")
    field(ZNAM, "FAULT!")
    field(DTYP, "asynUInt32Digital")
    field(INP,  "@asynMask($(PORT_NAME)-READ, 4, 0x0002, 1000)MODBUS_DATA")
    field(ZSV, "MAJOR")
}

record(bi, "$(P)$(R)ThermalFault") {
    field(DESC, "Thermal Fault")
    field(ONAM, "Fault!")
    field(DTYP, "asynUInt32Digital")
    field(INP,  "@asynMask($(PORT_NAME)-READ, 4, 0x0004, 1000)MODBUS_DATA")
    field(ZSV, "MAJOR")
}

record(bi, "$(P)$(R)IntCommFault") {
    field(DESC, "Internal Communication Fault")
    field(ONAM, "Fault!")
    field(DTYP, "asynUInt32Digital")
    field(INP,  "@asynMask($(PORT_NAME)-READ, 4, 0x0008, 1000)MODBUS_DATA")
    field(ZSV, "MAJOR")
}

record(bi, "$(P)$(R)ExtCommFault") {
    field(DESC, "External Communication Fault. Do Reset")
    field(ZNAM, "OK")
    field(ONAM, "Fault!")
    field(DTYP, "asynUInt32Digital")
    field(INP,  "@asynMask($(PORT_NAME)-READ, 4, 0x0010, 1000)MODBUS_DATA")
    field(ZSV, "MAJOR")
}

record(bi, "$(P)$(R)PsFault") {
    field(DESC, "Fault in 24V Power Supply. Do Reset")
    field(ONAM, "Fault!")
    field(DTYP, "asynUInt32Digital")
    field(INP,  "@asynMask($(PORT_NAME)-READ, 4, 0x0020, 1000)MODBUS_DATA")
    field(OSV, "MAJOR")
}
